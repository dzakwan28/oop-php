<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    echo "<h3>OOP PHP</h3>";

    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->jenis . "<br>"; 
    echo "legs : " . $sheep->property_legs . "<br>"; 
    echo "Cold blooded : " . $sheep->cold_blooded . "<br>". "<br>"; 

    $frog = new Frog("buduk");

    echo "Name : " . $frog->jenis . "<br>"; 
    echo "legs : " . $frog->property_legs . "<br>"; 
    echo "Cold blooded : " . $frog->cold_blooded . "<br>"; 
    echo $frog->suarakodok() . "<br>";

    $ape = new Ape("kera sakti");

    echo "Name : " . $ape->jenis . "<br>"; 
    echo "legs : " . $ape->property_legs . "<br>"; 
    echo "Cold blooded : " . $ape->cold_blooded . "<br>";
    echo $ape->suarakera(); 
?>